# Atelier Virtualisation VMWare

## Lab 2

Correcteur: Mr Pierre Cabantous

Elèves: Amrou Habibeh - Guillaume Ros

<div style="page-break-after: always;"></div>

## Table des matières

<div style="page-break-after: always;"></div>

## Plan d'adressage du Lab

![Plan d'adressage](assets/images/plan_adressage.png)

<div style="page-break-after: always;"></div>

## Configurations

### 1. Mise en place du partage FreeNAS

![Config NAS](assets/images/NFS2.png)

## Problèmes rencontrés

![Erreur vsphere](assets\images\NSF.png)