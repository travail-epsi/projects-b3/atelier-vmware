# Atelier Virtualisation VMWare

## Lab 1

Correcteur: Mr Pierre Cabantous

Elèves: Amrou Habibeh - Guillaume Ros

<div style="page-break-after: always;"></div>

## Table des matières

- [Atelier Virtualisation VMWare](#atelier-virtualisation-vmware)
  - [Lab 1](#lab-1)
  - [Table des matières](#table-des-matières)
  - [Plan d'adressage du Lab](#plan-dadressage-du-lab)
  - [Configurations](#configurations)
    - [1. Installation de la machine ESXI](#1-installation-de-la-machine-esxi)
    - [2. Installation de Vcenter](#2-installation-de-vcenter)
      - [- Détails de déploiement](#--détails-de-déploiement)
      - [- Détails du Datastore](#--détails-du-datastore)
      - [- Paramètres réseau](#--paramètres-réseau)
    - [3. Installation de Vcenter Appliance](#3-installation-de-vcenter-appliance)
      - [- Détails Réseau](#--détails-réseau)
      - [- Détails d'Appliance](#--détails-dappliance)
      - [- Détails SSO](#--détails-sso)
    - [Problèmes rencontrés](#problèmes-rencontrés)

<div style="page-break-after: always;"></div>

## Plan d'adressage du Lab

![Plan d'adressage](assets/images/plan_adressage.png)

<div style="page-break-after: always;"></div>

## Configurations

### 1. Installation de la machine ESXI

- 12.5 GB de RAM à l'installation (passage à 8 plus tard)

- 2 Processeurs (1 physique et 2 cores)
  
- 300 GB de stockage
  
- Une carte réseau NAT

- Une carte réseau Vmotion

- Une carte réseau SAN

<div style="page-break-after: always;"></div>

### 2. Installation de Vcenter

#### - Détails de déploiement

- ESXI host: 192.168.2.3
- Taille de déploiement: Tiny

#### - Détails du Datastore

- datastore 1, tiny

#### - Paramètres réseau

- Réseau VM Network
- IPV4
- Static
- IP: 192.168.2.100/24
- DNS: 192.168.2.10,192.168.2.2

<div style="page-break-after: always;"></div>

### 3. Installation de Vcenter Appliance

#### - Détails Réseau

- Static
- IPV4
- Hostname: photon-machine
- IP: 192.168.2.100/24
- Gateway: 192.168.2.2
- DNS Serveurs: 192.168.2.2,192.168.2.100
  
#### - Détails d'Appliance

- Synchro temps: fr.pool.ntp.org
- SSH: Activé
  
#### - Détails SSO

- Nom de domaine: vsphere.local
- Nom d'utilisateur: administrator

### Problèmes rencontrés

A la première installation, à l'étape 2, l'installation a planté à 5%. A chaque retry, le message d'erreur nous disait de recommencer l'installation de 0. Voici le message d'erreur

![Message d'erreur](assets/images/mstsc_xgnrpXqip9.png)

On suppose que cette erreur pourrait être liée à la synchronisation avec le serveur temporel qui est bloqué par les pare-feux de l'école. Plusieurs groupes ayant un PC connecté au réseau de l'EPSI ont eu le même problème. 
Cenpendant, pas tous les groupes ont eu d'erreur à cette étape là.