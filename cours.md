# Cours Virtualisation

- [Cours Virtualisation](#cours-virtualisation)
  - [Réseau Vcenter](#réseau-vcenter)
    - [Les PG de type VMKernel](#les-pg-de-type-vmkernel)
    - [Les Vswitch](#les-vswitch)
  - [Le Stockage](#le-stockage)
    - [Stockage Partagé](#stockage-partagé)

## Réseau Vcenter

Par défaut, sur esxi --> Virtual Switch + Port group qui s'appelle VMNETWORK.
Le virtual switch est attaché à une carte réseau physique.
La machine virtuelle est attachée au Port group

### Les PG de type VMKernel

- Management --> Besoin d'une ip pour l'esxi
- Stockage --> Besoin d'une ip pour l'esxi
- VMotion --> Besoin d'une ip pour l'esxi

### Les Vswitch

Peut être relié à une carte réseau physique mais ce n'est pas obligatoire.
On peut rattacher plusieurs PG à un VSwitch

## Le Stockage

Pour mettre en place un cluster, il faut que les VM soient stockées sur un stockage externe.

Tout stockage sur esxi est un datastore (disque en plus, Stockage externe, partition)

### Stockage Partagé

- Utilisation du protocole NFS qui fonctionne par IP (utilisation pour les NAS).

- Utilisation du protocole ISCSI qui monte un disque comme si c'était un disque local. Ca permet au serveur esxi (ou autre serveur) de gérer directement le formatage du disque.
  
  LUN: C'est un id qui correspond à un volume qui correspond à un ou plusieurs disques. Un LUN est une target
  
  Initiateur: Brique logicielle ou matérielle qui permet de lancer la connexion ISCI

- Utilisation du protocole Fibre Channel qui n'utilise pas de cable fibre. C'est un protocole à part qui n'utilise pas l'adressage IP. Ce protocole demande du matériel à part. On peut l'utiliser avec des connexions fibre, ethernet, cuivre ou propriétaire.
